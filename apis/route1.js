var expressRoute1 = require('express')
var route1 = expressRoute1.Router()

//create this middleware for the prayer app

route1.use(function timeLog (req,res,next) {
   console.log('Time: ', Date.now())
   next()
})


route1.get('/', function (req, res) {
   res.send('birds home page')

})

route1.get('/about', function (req, res) {
   res.send('about page for birds')

})


module.exports = route1
